package org.sid.dao;

import org.sid.entites.Classe;
import org.springframework.data.jpa.repository.JpaRepository;





public interface ClasseRepository extends JpaRepository<Classe,String>{

}
