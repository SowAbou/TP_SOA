package org.sid.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import org.sid.entites.Faculte;



public interface FaculteRepository extends JpaRepository<Faculte, String>{

}
