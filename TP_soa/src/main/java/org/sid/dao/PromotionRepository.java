package org.sid.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import org.sid.entites.Promotion;



public interface PromotionRepository extends JpaRepository<Promotion, String>{

}
