package org.sid.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import org.sid.entites.Etudiant;



public interface EtudiantRepository extends JpaRepository<Etudiant,String>{

}
