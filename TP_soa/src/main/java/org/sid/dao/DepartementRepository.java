package org.sid.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import org.sid.entites.Departement;

public interface DepartementRepository extends JpaRepository<Departement, Long>{

}
