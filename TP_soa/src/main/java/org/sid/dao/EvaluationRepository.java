package org.sid.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import org.sid.entites.Evaluation;



public interface EvaluationRepository  extends JpaRepository<Evaluation, String>{

}
