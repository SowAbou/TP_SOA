package org.sid.dao;

import org.springframework.data.jpa.repository.JpaRepository;


import org.sid.entites.Societe;

public interface SocieteRepository extends JpaRepository<Societe, Long>{

}
