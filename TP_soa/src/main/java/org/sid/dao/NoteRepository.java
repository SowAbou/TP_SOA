package org.sid.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import org.sid.entites.Note;



public interface NoteRepository extends JpaRepository<Note, String>{

}
