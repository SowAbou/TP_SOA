package org.sid;

import java.util.stream.Stream;

import org.sid.dao.EtudiantRepository;
import org.sid.dao.FaculteRepository;
import org.sid.dao.SocieteRepository;
import org.sid.entites.Etudiant;
import org.sid.entites.Faculte;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;




@SpringBootApplication
public class FormationApplication implements CommandLineRunner{
	@Autowired
	EtudiantRepository repository ;

	public static void main(String[] args) {
		  SpringApplication.run(FormationApplication.class, args);
		
		
	}

	@Override
	public void run(String... args) throws Exception {
		 repository.save(new Etudiant("ref001", null, "Sow", "Abou", "abou@gmail.com", null)) ;
		 repository.save(new Etudiant("ref002", null, "Kinde", "Mamadou", "Kinde@gmail.com", null)) ;
		 repository.save(new Etudiant("ref003", null, "Sakiné", "khourr", "sax@gmail.com", null)) ;
		 repository.findAll().forEach(s->{
			 System.out.println(s.getPrenom()+" "+s.getNom()) ;
		 }) ;
		
	}
}
