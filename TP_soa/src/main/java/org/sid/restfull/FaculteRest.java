package org.sid.restfull;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import org.sid.dao.FaculteRepository;
import org.sid.entites.Faculte;





@RestController
public class FaculteRest {

	@Autowired
	private FaculteRepository faculteRepository;
	
	@RequestMapping(value = "/listFaculte",method=RequestMethod.GET)
	public List<Faculte> listContacts(){
		return faculteRepository.findAll();
	}
	
	@RequestMapping(value = "/saveFaculte",method=RequestMethod.POST)
	public Faculte saveContact(@RequestBody Faculte c){
		return faculteRepository.save(c);
	}
	
	@RequestMapping(value = "/editFaculte",method=RequestMethod.PUT)
	public Faculte updateContact(@RequestBody Faculte c){
		return faculteRepository.saveAndFlush(c);
	}
	
	@RequestMapping(value = "/deleteFaculte/{ref}", method = RequestMethod.DELETE)
	public boolean deleteContact(@PathVariable("ref") String id){		
		faculteRepository.delete(id);
		return true;
	}
}
