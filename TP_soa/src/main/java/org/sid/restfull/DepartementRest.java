package org.sid.restfull;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import org.sid.dao.DepartementRepository;
import org.sid.entites.Departement;




@RestController
public class DepartementRest {
	@Autowired
	private DepartementRepository departementRepository;
	
	@RequestMapping(value = "/listDepartement",method=RequestMethod.GET)
	public List<Departement> listContacts(){
		return departementRepository.findAll();
		
	}
	
	@RequestMapping(value = "/departement/{ref}",method=RequestMethod.GET)
	public Departement getContactByRef(@PathVariable("ref")Long ref){
		return departementRepository.getOne(ref) ;
	}
	@RequestMapping(value = "/saveDepartement",method=RequestMethod.POST)
	public Departement saveContact(@RequestBody Departement c){
		return departementRepository.save(c);
	}
	@RequestMapping(value = "/editDepartement",method=RequestMethod.PUT)
	public Departement updateContact(@RequestBody Departement c){
		return departementRepository.saveAndFlush(c);
	}
	@RequestMapping(value = "/deleteDepartement/{ref}", method = RequestMethod.DELETE)
	public boolean deleteContact(@PathVariable("ref") Long id){		
		departementRepository.delete(id);;
		return true;
	}
}
