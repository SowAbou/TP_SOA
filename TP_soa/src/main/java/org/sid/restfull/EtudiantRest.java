package org.sid.restfull;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import org.sid.dao.EtudiantRepository;
import org.sid.entites.Etudiant;




public class EtudiantRest {
	@Autowired
	private EtudiantRepository etudiantRepository;
	
	@RequestMapping(value = "/etudiants",method=RequestMethod.GET)
	public List<Etudiant> listContacts(){
		return etudiantRepository.findAll();
		
	}
	
	@RequestMapping(value = "/etudiants/{ref}",method=RequestMethod.GET)
	public Etudiant getContactByRef(@PathVariable("ref")String ref){
		return etudiantRepository.getOne(ref);
	}
	@RequestMapping(value = "/etudiants/",method=RequestMethod.POST)
	public Etudiant saveContact(@RequestBody Etudiant c){
		return etudiantRepository.save(c);
	}
	@RequestMapping(value = "/editEC",method=RequestMethod.PUT)
	public Etudiant updateContact(@RequestBody Etudiant c){
		return etudiantRepository.saveAndFlush(c);
	}
	@RequestMapping(value = "/deleteEC/{ref}", method = RequestMethod.DELETE)
	public boolean deleteContact(@PathVariable("ref") String id){		
		etudiantRepository.delete(id);
		return true;
	}
}
